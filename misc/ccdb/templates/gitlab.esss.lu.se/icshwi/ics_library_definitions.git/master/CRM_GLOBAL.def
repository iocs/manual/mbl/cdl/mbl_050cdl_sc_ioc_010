###################################### TS2###############################################
################################# Version: 1.0 ##########################################
# Author:  Emilio Asensi
# Date:    30-11-2022
# Version: v1.0
# Cloned from TS2_GLOBAL, added night mode interlock conditions
################################# Version: 1.1 ##########################################
# Author:  Emilio Asensi
# Date:    04-10-2023
# Version: v1.1
# Added TDK/BE selector pvs


############################
#  STATUS BLOCK
############################
define_status_block()

######################################################
#Interlock's CTL
######################################################
add_digital("VacOK1",           ARCHIVE=True,           PV_DESC="VacumOK",                         PV_ONAM="True",       PV_ZNAM="False")
add_digital("VacOK2",           ARCHIVE=True,           PV_DESC="VacumOK",                         PV_ONAM="True",       PV_ZNAM="False")
add_digital("CDS_Cryo_Ready",   ARCHIVE=True,           PV_DESC="Interlock in CTL Cryo Ready",     PV_ONAM="True",       PV_ZNAM="False")
add_digital("CDS_Cryo_OK",      ARCHIVE=True,           PV_DESC="Interlock in CTL Cryo OK",        PV_ONAM="True",       PV_ZNAM="False")
add_digital("VacOK_PT895",                              PV_DESC="VacuumTurboPump",                 PV_ONAM="True",       PV_ZNAM="False")
add_digital("VacOK_PT893",                              PV_DESC="VacuumTurboPump",                 PV_ONAM="True",       PV_ZNAM="False")
add_digital("VacOK_PT892",                              PV_DESC="VacuumTurboPump",                 PV_ONAM="True",       PV_ZNAM="False")
add_digital("VacNotOK",                                 PV_DESC="Signal VAC NOT OK",               PV_ONAM="True",       PV_ZNAM="False")

######################################################
#Endbox to interlocks in CTL
######################################################

add_digital("VacOK_VGP70",              PV_DESC="Pirani vac OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("VacOK_VGC71",              PV_DESC="Cold cathode vac ok",       PV_ONAM="True",           PV_ZNAM="False")


#Operation modes
add_digital("OpMode_Auto",              PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",            PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Forced",            PV_DESC="Operation Mode Forced",     PV_ONAM="True",           PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",           PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",  PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",            PV_DESC="Inhibit Force Mode",        PV_ONAM="InhibitForce",   PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",             PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking", PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",           PV_DESC="Group Interlock",          PV_ONAM="True",             PV_ZNAM="False")
add_digital("StartInterlock",           ARCHIVE=True,                       PV_DESC="Start Interlock",  PV_ONAM="True",           PV_ZNAM="False")
add_digital("StopInterlock",            ARCHIVE=True,                       PV_DESC="Stop Interlock",   PV_ONAM="True",           PV_ZNAM="False")

#Block Icon controls
add_digital("EnableBlkOpen",            PV_DESC="Enable Block Open Button",  PV_ONAM="True",            PV_ZNAM="False")
add_digital("EnableBlkClose",           PV_DESC="Enable Block Close Button", PV_ONAM="True",            PV_ZNAM="False")

#for OPI visualisation
add_digital("EnableAutoBtn",            PV_DESC="Enable Auto Button",        PV_ONAM="True",            PV_ZNAM="False")
add_digital("EnableManualBtn",          PV_DESC="Enable Manual Button",      PV_ONAM="True",            PV_ZNAM="False")
add_digital("EnableForcedBtn",          PV_DESC="Enable Force Button",       PV_ONAM="True",            PV_ZNAM="False")
add_string("InterlockMsg",              39,                                  PV_NAME="InterlockMsg",    PV_DESC="Interlock Message")
add_digital("NormallyOpen",             PV_DESC="PV is Normally opened")

#for TDK LAMBDA
add_digital("TDK_lambda_RB",            PV_DESC="Read back TDK lambda",      PV_ONAM="True",            PV_ZNAM="False")
add_digital("ICS_QHD_RB",               PV_DESC="Read back ICS QHD",         PV_ONAM="True",            PV_ZNAM="False")
add_digital("ICS_Itest_RB",             PV_DESC="Read back ICS Itest",       PV_ONAM="True",            PV_ZNAM="False")
add_digital("TDK_Itest_RB",             PV_DESC="Read back TDK with Itest",  PV_ONAM="True",            PV_ZNAM="False")

#for TDK16 to TDK12 
add_digital("TDK16_RB",                 PV_DESC="Read back TDK16 Selection",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("TDK12_RB",                 PV_DESC="Read back TDK12 Selection",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("BE220_RB",                 PV_DESC="Read back BE220 Selection",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("TDK_BE220_RB",             PV_DESC="Read back TDK + BE220 Selection",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("SingleLoad_RB",            PV_DESC="Read back Single Load Selection",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("DoubleLoad_RB",            PV_DESC="Read back Double Load Selection",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("DoubleLoad_En",            PV_DESC="Double Load Enabled for Faceplate",    PV_ONAM="True",           PV_ZNAM="False")

#Filters
add_digital("Filter_enabled")
add_analog("Filter_measValue", "REAL",  PV_PREC="2")
add_analog("Filter_filterValue", "REAL",  PV_PREC="2")
add_analog("Filter_interval", "INT")
add_analog("Filter_buffer", "INT")
add_analog("Filter_calc", "REAL",  PV_PREC="2")

#Timer
add_digital("Timer_enabled")
add_digital("Timer_triggered")
add_analog("Timer_period", "INT")
add_analog("Timer_running", "INT")


#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",             PV_ONAM="True",           PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                              PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                              PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",              PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")

#Alarm signals


add_major_alarm("IO_Error",            "IO Error",                          PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")

###########################
#  Major Alarms
############################

define_plc_array("Major_Alarms")
add_major_alarm("Major_Alarm_001","Major Alarm 1",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_002","Major Alarm 2",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_003","Major Alarm 3",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_004","Major Alarm 4",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_005","Major Alarm 5",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_006","Major Alarm 6",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_007","Major Alarm 7",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_008","Major Alarm 8",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_009","Major Alarm 9",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_010","Major Alarm 10",PV_ONAM="NominalState",PV_ZNAM="Tripped")
end_plc_array()

############################
#  Minor Alarms
############################

define_plc_array("Minor_Alarms")
add_minor_alarm("Minor_Alarm_001","Minor Alarm 1", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_002","Minor Alarm 2", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_003","Minor Alarm 3", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_004","Minor Alarm 4", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_005","Minor Alarm 5", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_006","Minor Alarm 6", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_007","Minor Alarm 7", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_008","Minor Alarm 8", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_009","Minor Alarm 9", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_010","Minor Alarm 10", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
end_plc_array()


#Spare IOs
add_digital("ReadDI_Others_001", ARCHIVE=True)
add_digital("ReadDI_Others_002", ARCHIVE=True)
add_digital("ReadDI_Others_003", ARCHIVE=True)
add_digital("ReadDI_Others_004", ARCHIVE=True)
add_digital("ReadDI_Others_005", ARCHIVE=True)
add_digital("ReadDI_Others_006", ARCHIVE=True)
add_digital("ReadDI_Others_007", ARCHIVE=True)
add_digital("ReadDI_Others_008", ARCHIVE=True)
add_digital("ReadDI_Others_009", ARCHIVE=True)
add_digital("ReadDI_Others_010", ARCHIVE=True)

add_analog("readAI_Others_001","REAL",  PV_PREC="2")
add_analog("readAI_Others_002","REAL",  PV_PREC="2")
add_analog("readAI_Others_003","REAL",  PV_PREC="2")
add_analog("readAI_Others_004","REAL",  PV_PREC="2")
add_analog("readAI_Others_005","REAL",  PV_PREC="2")
add_analog("readAI_Others_006","REAL",  PV_PREC="2")
add_analog("readAI_Others_007","REAL",  PV_PREC="2")
add_analog("readAI_Others_008","REAL",  PV_PREC="2")
add_analog("readAI_Others_009","REAL",  PV_PREC="2")
add_analog("readAI_Others_010","REAL",  PV_PREC="2")
add_analog("readAI_INT_001","INT")
add_analog("readAI_INT_002","INT")
add_analog("readAI_INT_003","INT")
add_analog("readAI_INT_004","INT")
add_analog("readAI_INT_005","INT")
add_analog("readAI_INT_006","INT")
add_analog("readAI_INT_007","INT")
add_analog("readAI_INT_008","INT")
add_analog("readAI_INT_009","INT")
add_analog("readAI_INT_010","INT")

#Communication signals
add_digital("StatusFromPSS")
add_digital("StatusFromTICP")
add_digital("StatusFromLPS")
add_digital("StatusFromMC")
add_digital("StatusFromRFC")
add_digital("StatusFromSKID")
add_digital("StatusFrom001")
add_digital("StatusFrom002")
add_digital("StatusFrom003")
add_analog("PID485tsp_TICP","REAL",  PV_PREC="2")
add_analog("readAI_TICP_001","REAL",  PV_PREC="2")
add_analog("readAI_TICP_002","REAL",  PV_PREC="2")
add_analog("readAI_TICP_003","REAL",  PV_PREC="2")

#CryomoduleReady_Limits
add_digital("Cond_Temp01OK")
add_digital("Cond_Temp02OK")
add_digital("Cond_Temp03OK")
add_digital("Cond_Temp04OK")
add_digital("Cond_Temp05OK")
add_digital("Cond_Pres01OK")
add_digital("Cond_Pres02OK")
add_digital("Cond_Pres03OK")
add_digital("Cond_Pres04OK")
add_digital("Cond_Pres05OK")
add_digital("Cond_Lvl01OK")
add_digital("Cond_Lvl02OK")
add_digital("Cond_Lvl03OK")
add_digital("Cond_Flow01OK")
add_digital("Cond_Flow02OK")
add_digital("Cond_Flow03OK")
add_digital("Cond_Flow04OK")
add_digital("Cond_Flow05OK")
add_digital("ColdCondLevelOK",         PV_DESC="OK Cold Conditioning Level")
add_digital("ColdCondPressureOK",      PV_DESC="OK Cold Conditioning Pressure")
add_digital("ColdCondCavTempOK",       PV_DESC="OK Cold Conditioning Cav Temp")
add_digital("CondFPCTempOK",           PV_DESC="OK Conditioning FPC Temp")
add_digital("CondWtrCTempOK",          PV_DESC="OK Conditioning Water Temp")
add_digital("CondWtrCFlowOK",          PV_DESC="OK Conditioning Water Flow")

add_analog("Cond_Temp01",           "REAL",  PV_PREC="2",           PV_DESC="Temperature condition")
add_analog("Cond_Temp02",           "REAL",  PV_PREC="2",           PV_DESC="Temperature condition")
add_analog("Cond_Temp03",           "REAL",  PV_PREC="2",           PV_DESC="Temperature condition")
add_analog("Cond_Temp04",           "REAL",  PV_PREC="2",           PV_DESC="Temperature condition")
add_analog("Cond_Temp05",           "REAL",  PV_PREC="2",           PV_DESC="Temperature condition")
add_analog("Cond_Pres01",           "REAL",  PV_PREC="2",           PV_DESC="Pressure condition")
add_analog("Cond_Pres02",           "REAL",  PV_PREC="2",           PV_DESC="Pressure condition")
add_analog("Cond_Pres03",           "REAL",  PV_PREC="2",           PV_DESC="Pressure condition")
add_analog("Cond_Pres04",           "REAL",  PV_PREC="2",           PV_DESC="Pressure condition")
add_analog("Cond_Pres05",           "REAL",  PV_PREC="2",           PV_DESC="Pressure condition")
add_analog("Cond_Lvl01",            "REAL",  PV_PREC="2",           PV_DESC="Level condition")
add_analog("Cond_Lvl02",            "REAL",  PV_PREC="2",           PV_DESC="Level condition")
add_analog("Cond_Lvl03",            "REAL",  PV_PREC="2",           PV_DESC="Level condition")
add_analog("Cond_Flow01",           "REAL",  PV_PREC="2",           PV_DESC="Flow condition")
add_analog("Cond_Flow02",           "REAL",  PV_PREC="2",           PV_DESC="Flow condition")
add_analog("Cond_Flow03",           "REAL",  PV_PREC="2",           PV_DESC="Flow condition")
add_analog("Cond_Flow04",           "REAL",  PV_PREC="2",           PV_DESC="Flow condition")
add_analog("Cond_Flow05",           "REAL",  PV_PREC="2",           PV_DESC="Flow condition")
add_analog("CCondLevelMIN_st",      "REAL",  PV_PREC="2",           PV_DESC="Min Cold Conditioning Level")
add_analog("CCondPressureMAX_st",   "REAL",  PV_PREC="2",           PV_DESC="Max Cold Conditioning Pressure")
add_analog("CCondCavTempMAX_st",    "REAL",  PV_PREC="2",           PV_DESC="Max Cold Conditioning Cav Temp")
add_analog("CondFPCTempMAX_st",     "REAL",  PV_PREC="2",           PV_DESC="Max Conditioning FPC Temp")
add_analog("CondWtrCTempMAX_st",    "REAL",  PV_PREC="2",           PV_DESC="Max Conditioning Water Temp")
add_analog("CondWtrCFlowMAX_st",    "REAL",  PV_PREC="2",           PV_DESC="Max Conditioning Water Flow")
add_analog("CondWtrCFlowMIN_st",    "REAL",  PV_PREC="2",           PV_DESC="Min Conditioning Water Flow")
add_analog("WtrCFS_F_st",           "REAL",  PV_PREC="2",           PV_DESC="Water FS filter time")

#Interlock conditions
add_analog("NightPressure_st",      "REAL",  PV_PREC="2",           PV_DESC="Pressure value to trigger Night prot")
add_analog("NightLevel_st",         "REAL",  PV_PREC="2",           PV_DESC="Level value to trigger Night prot")
add_analog("NightCond01_st",        "REAL",  PV_PREC="2",           PV_DESC="Value to trigger Night mode protection")
add_analog("NightCond02_st",        "REAL",  PV_PREC="2",           PV_DESC="Value to trigger Night mode protection")
add_analog("NightCond03_st",        "REAL",  PV_PREC="2",           PV_DESC="Value to trigger Night mode protection")
add_digital("NightCondDI01_st",	                                    PV_DESC="Contact to trigger Night mode protection")
add_digital("NightCondDI02_st",	                                    PV_DESC="Contact to trigger Night mode protection")
add_digital("NightCondDI03_st",	                                    PV_DESC="Contact to trigger Night mode protection")

#Diagnostics
#Local modules connection state
add_digital("CPU_Connected",                         PV_DESC="LM Siemens CPU connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("CP_Connected",                          PV_DESC="LM Siemens CP connected",       PV_ONAM="Connected",        PV_ZNAM="Disconnected")

#Local modules connection state
add_digital("CPU_Status",                            PV_DESC="LM Siemens CPU state",          PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("CP_Status",                             PV_DESC="LM Siemens CP state",           PV_ONAM="OK",               PV_ZNAM="NOK")

#Local modules connection state
define_plc_array("LM_Connected")
add_digital("LM1_Connected",                            PV_DESC="Siemens LM1 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM2_Connected",                            PV_DESC="Siemens LM2 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM3_Connected",                            PV_DESC="Siemens LM3 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM4_Connected",                            PV_DESC="Siemens LM4 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM5_Connected",                            PV_DESC="Siemens LM5 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM6_Connected",                            PV_DESC="Siemens LM6 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM7_Connected",                            PV_DESC="Siemens LM7 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM8_Connected",                            PV_DESC="Siemens LM8 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM9_Connected",                            PV_DESC="Siemens LM9 connected",      PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM10_Connected",                           PV_DESC="Siemens LM10 connected",     PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM11_Connected",                           PV_DESC="Siemens LM11 connected",     PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM12_Connected",                           PV_DESC="Siemens LM12 connected",     PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM13_Connected",                           PV_DESC="Siemens LM13 connected",     PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("LM14_Connected",                           PV_DESC="Siemens LM14 connected",     PV_ONAM="Connected",        PV_ZNAM="Disconnected")
end_plc_array()

#Local modules connection state
define_plc_array("LM_Status")
add_digital("LM1_Status",                               PV_DESC="Siemens LM1 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM2_Status",                               PV_DESC="Siemens LM2 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM3_Status",                               PV_DESC="Siemens LM3 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM4_Status",                               PV_DESC="Siemens LM4 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM5_Status",                               PV_DESC="Siemens LM5 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM6_Status",                               PV_DESC="Siemens LM6 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM7_Status",                               PV_DESC="Siemens LM7 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM8_Status",                               PV_DESC="Siemens LM8 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM9_Status",                               PV_DESC="Siemens LM9 Status",         PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM10_Status",                              PV_DESC="Siemens LM10 Status",        PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM11_Status",                              PV_DESC="Siemens LM11 Status",        PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM12_Status",                              PV_DESC="Siemens LM12 Status",        PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM13_Status",                              PV_DESC="Siemens LM13 Status",        PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("LM14_Status",                              PV_DESC="Siemens LM14 Status",        PV_ONAM="OK",               PV_ZNAM="NOK")
end_plc_array()

#RIO1 connection state
add_digital("RIO1_Connected",                        PV_DESC="RIO1 connected",                PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("RIO1_Status",                           PV_DESC="RIO1 state",                    PV_ONAM="OK",               PV_ZNAM="NOK")

#RIO2 connection state
add_digital("RIO2_Connected",                        PV_DESC="RIO2 connected",                PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("RIO2_Status",                           PV_DESC="RIO2 state",                    PV_ONAM="OK",               PV_ZNAM="NOK")

#RIO3 connection state
add_digital("RIO3_Connected",                        PV_DESC="RIO3 connected",                PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("RIO3_Status",                           PV_DESC="RIO3 state",                    PV_ONAM="OK",               PV_ZNAM="NOK")

#RIO4 connection state
add_digital("RIO4_Connected",                        PV_DESC="RIO4 connected",                PV_ONAM="Connected",        PV_ZNAM="Disconnected")
add_digital("RIO4_Status",                           PV_DESC="RIO4 state",                    PV_ONAM="OK",               PV_ZNAM="NOK")

#Timers and Filters
add_digital("T0_enabled")
add_digital("T0_triggered")
add_analog("T0_running", "INT")
add_digital("T1_enabled")
add_digital("T1_triggered")
add_analog("T1_running", "INT")
add_digital("T2_enabled")
add_digital("T2_triggered")
add_analog("T2_running", "INT")
add_digital("T3_enabled")
add_digital("T3_triggered")
add_analog("T3_running", "INT")
add_digital("T4_enabled")
add_digital("T4_triggered")
add_analog("T4_running", "INT")
add_digital("T5_enabled")
add_digital("T5_triggered")
add_analog("T5_running", "INT")
add_digital("F0_enabled")
add_digital("F1_enabled")
add_digital("F2_enabled")
add_digital("F3_enabled")
add_digital("F4_enabled")
add_digital("F5_enabled")
add_analog("F0_outValue", "REAL",  PV_PREC="2")
add_analog("F1_outValue", "REAL",  PV_PREC="2")
add_analog("F2_outValue", "REAL",  PV_PREC="2")
add_analog("F3_outValue", "REAL",  PV_PREC="2")
add_analog("F4_outValue", "REAL",  PV_PREC="2")
add_analog("F5_outValue", "REAL",  PV_PREC="2")

#Water Flushing Digitals
add_digital("Flushing_ON",  	    ARCHIVE=True,           PV_DESC="WF Flush ON",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("Flushing_OFF",  	    ARCHIVE=True,           PV_DESC="WF Flush OFF",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Auto",       ARCHIVE=True,           PV_DESC="WF Flush Mode Auto",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Manual",     ARCHIVE=True,           PV_DESC="WF Flush  Mode Manual",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_OpMode_Forced",     ARCHIVE=True,           PV_DESC="WF Flush  Mode Forced",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_01",      ARCHIVE=True,           PV_DESC="WF Conditions reached",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_02",      ARCHIVE=True,           PV_DESC="WF Conditions reached",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_03",      ARCHIVE=True,           PV_DESC="WF Conditions reached",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_FlushCond_04",      ARCHIVE=True,           PV_DESC="WF Conditions reached",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_01",         ARCHIVE=True,           PV_DESC="WF Circuit 1 Blowed",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_02",         ARCHIVE=True,           PV_DESC="WF Circuit 2 Blowed",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_03",         ARCHIVE=True,           PV_DESC="WF Circuit 3 Blowed",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("WF_Blowed_04",         ARCHIVE=True,           PV_DESC="WF Circuit 4 Blowed",        PV_ONAM="True",           PV_ZNAM="False")

#Water Flushing Timers
add_time("WF_Wait_Timer_ET_01",                          PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_02",                          PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_03",                          PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")
add_time("WF_Wait_Timer_ET_04",                          PV_DESC="Elapsed time before Water flush",  PV_EGU="ms")


add_major_alarm("WF_TT_Error",          "WF Temperature Error",         PV_ZNAM="NominalState")
add_major_alarm("WF_PT_Error",          "WF Pressure Error",            PV_ZNAM="NominalState")
add_major_alarm("WF_YCV_Error",         "WF Valve pos Error",           PV_ZNAM="NominalState")
add_major_alarm("WF_FT_Error",          "WF Flow Error",                PV_ZNAM="NominalState")
add_major_alarm("WF_General_Error",     "WF General Error",             PV_ZNAM="NominalState")
add_major_alarm("WF_01_Error",          "Water Error 01",               PV_ZNAM="NominalState")
add_major_alarm("WF_02_Error",          "Water Error 02",               PV_ZNAM="NominalState")
add_major_alarm("WF_03_Error",          "Water Error 03",               PV_ZNAM="NominalState")
add_major_alarm("WF_04_Error",          "Water Error 04",               PV_ZNAM="NominalState")
add_major_alarm("WF_AirLeackage",       "WF Air Leackage",              PV_ZNAM="NominalState")
add_major_alarm("WF_WaterLeackage",     "WF WaterLeackage",             PV_ZNAM="NominalState")
add_major_alarm("WF_WaterOverPressure", "WF Water Over Pressure",       PV_ZNAM="NominalState")
add_major_alarm("WF_AirOverPressure",   "WF Air Over Pressure",         PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_01",  "WF Freezing Circuit 1",        PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_02",  "WF Freezing Circuit 2",        PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_03",  "WF Freezing Circuit 3",         PV_ZNAM="NominalState")
add_major_alarm("WF_WaterFreezing_04",  "WF Freezing Circuit 4",        PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                     PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",                   PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",                    PV_DESC="CMD: Force Mode")
add_digital("Cmd_ManuOpen",                 PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",                PV_DESC="CMD: Manual Close")
add_digital("Cmd_ForceOpen",                PV_DESC="CMD: Force Open")
add_digital("Cmd_ForceClose",               PV_DESC="CMD: Force Close")

add_digital("Cmd_AckAlarm",                 PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_AckAllAlarm",              PV_DESC="CMD: Acknowledge All Alarms")
add_digital("Cmd_AckFSAlarm",               PV_DESC="CMD: Acknowledge Full Stop Interlocks")
add_digital("Cmd_AckTSAlarm",               PV_DESC="CMD: Acknowledge Temp Stop Interlocks")
add_digital("Cmd_AckSIAlarm",               PV_DESC="CMD: Acknowledge Start Interlocks")
add_digital("Cmd_AckMjAlarm",               PV_DESC="CMD: Acknowledge Major Alarms")
add_digital("Cmd_AckMnAlarm",               PV_DESC="CMD: Acknowledge Minor Alarms")

#Water Flushing
add_digital("Cmd_WF_Auto",                  ARCHIVE=" 1Hz",		PV_DESC="CMD: Auto Mode")
add_digital("Cmd_WF_Manual",                ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Mode")
add_digital("Cmd_WF_Force",                 ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Mode")
add_digital("Cmd_WF_ManuOpen",              ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Open")
add_digital("Cmd_WF_ManuClose",             ARCHIVE=" 1Hz",		PV_DESC="CMD: Manual Close")
add_digital("Cmd_WF_ForceOpen",             ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Open")
add_digital("Cmd_WF_ForceClose",            ARCHIVE=" 1Hz",		PV_DESC="CMD: Force Close")
add_digital("Cmd_WF_AckAlarm",              ARCHIVE=" 1Hz",		PV_DESC="CMD: Acknowledge All Alarms")

add_digital("Cmd_ForceUnlock",              PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",                  PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",                PV_DESC="CMD: Unlock Device")

#for TDK LAMBDA
add_digital("Cmd_TDK_ON",                   PV_DESC="Enable TDK lambda",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_QHD_ON",                   PV_DESC="Enable ISC QHD",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_Itest_ON",                 PV_DESC="Enable Itest drivers",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_TDK_Itest_ON",             PV_DESC="Enable Itest + TDK drivers",   PV_ONAM="True",           PV_ZNAM="False")

#for TDK16 to TDK12 
add_digital("Cmd_TDK16_ON",               PV_DESC="Enable TDK16 Selection",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_TDK12_ON",               PV_DESC="Enable TDK12 Selection",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_BE220_ON",               PV_DESC="Enable BE220 Selection",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_TDK_BE220_ON",     PV_DESC="Enable TDK + BE220 Selection",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_SingleLoad_ON",    PV_DESC="Single Load activation Command",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cmd_DoubleLoad_ON",     PV_DESC="Double Load activation Command",        PV_ONAM="True",           PV_ZNAM="False")

add_digital("writeDI_Others_001")
add_digital("writeDI_Others_002")
add_digital("writeDI_Others_003")
add_digital("writeDI_Others_004")
add_digital("writeDI_Others_005")
add_digital("writeDI_Others_006")
add_digital("writeDI_Others_007")
add_digital("writeDI_Others_008")
add_digital("writeDI_Others_009")
add_digital("writeDI_Others_010")

add_analog("writeAI_Others_001","REAL", PV_PREC="2")
add_analog("writeAI_Others_002","REAL", PV_PREC="2")
add_analog("writeAI_Others_003","REAL", PV_PREC="2")
add_analog("writeAI_Others_004","REAL", PV_PREC="2")
add_analog("writeAI_Others_005","REAL", PV_PREC="2")
add_analog("writeAI_Others_006","REAL", PV_PREC="2")
add_analog("writeAI_Others_007","REAL", PV_PREC="2")
add_analog("writeAI_Others_008","REAL", PV_PREC="2")
add_analog("writeAI_Others_009","REAL", PV_PREC="2")
add_analog("writeAI_Others_010","REAL", PV_PREC="2")

add_analog("writeAI_INT_001","INT")
add_analog("writeAI_INT_002","INT")
add_analog("writeAI_INT_003","INT")
add_analog("writeAI_INT_004","INT")
add_analog("writeAI_INT_005","INT")
add_analog("writeAI_INT_006","INT")
add_analog("writeAI_INT_007","INT")
add_analog("writeAI_INT_008","INT")
add_analog("writeAI_INT_009","INT")
add_analog("writeAI_INT_010","INT")

#CryomoduleReady Conditions
add_digital("Cmd_ColdCond",              PV_DESC="CMD: Cold Conditioning")
add_digital("Cmd_WarmCond",              PV_DESC="CMD: Warm Conditioning")
add_digital("Cmd_RegCond",              PV_DESC="CMD: Regular Operations")
add_digital("cmd_CCondDefParam",		PV_DESC="Set default parameters Cold Cond")
add_digital("cmd_WCondDefParam",		PV_DESC="Set default parameters Warm Cond")
add_digital("cmd_XCondDefParam",		PV_DESC="Set default parameters X Cond")
add_digital("cmd_DefParam01",			PV_DESC="Set default parameters 01")
add_digital("cmd_DefParam02",			PV_DESC="Set default parameters 02")
add_digital("cmd_DefParam03",			PV_DESC="Set default parameters 03")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

add_digital("Parked_Crm",               PV_DESC="Cryomodule Parked",        PV_ONAM="True",           PV_ZNAM="False")


####################################
#  Endbox comming interlock signals#
####################################
add_digital("VacOK1_EB",  ARCHIVE=" 1Hz",          PV_DESC="VacumOK1_FromEndbox",                 PV_ONAM="True",         PV_ZNAM="False")
add_digital("VacOK2_EB",  ARCHIVE=" 1Hz",          PV_DESC="VacumOK2_FromEndbox",                 PV_ONAM="True",         PV_ZNAM="False")

#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",     PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",     PV_DESC="Device ID after Blockicon Open")
add_digital("P_retentivemode",ARCHIVE=" 1Hz",      PV_DESC="VacumOK1_FromEndbox",                 PV_ONAM="True",         PV_ZNAM="False")

#TS2 status from OPI
add_analog("TS2_Status",       "DINT",             PV_DESC="Test Stand 2 state")
add_analog("CM_Status",        "DINT",             PV_DESC="Cryomodule state")
add_analog("CDS_Status",       "DINT",             PV_DESC="CDS state")

add_digital("CryoOK")
add_digital("CryoReady")
add_digital("CryoModuleReady")
add_digital("MotorOK")
add_digital("PS-091")

#Communication signals
add_digital("StatusToPSS")
add_digital("StatusToTICP")
add_digital("StatusToLPS")
add_digital("StatusToMC")
add_digital("StatusToRFC")
add_digital("StatusToSKID")
add_digital("StatusTo001")
add_digital("StatusTo002")
add_digital("StatusTo003")

#Cryomodule Ready Limits
add_analog("MAX_Temp01","REAL", PV_PREC="2")
add_analog("MAX_Temp02","REAL", PV_PREC="2")
add_analog("MAX_Temp03","REAL", PV_PREC="2")
add_analog("MAX_Temp04","REAL", PV_PREC="2")
add_analog("MAX_Temp05","REAL", PV_PREC="2")
add_analog("MIN_Temp01","REAL", PV_PREC="2")
add_analog("MIN_Temp02","REAL", PV_PREC="2")
add_analog("MIN_Temp03","REAL", PV_PREC="2")
add_analog("MIN_Temp04","REAL", PV_PREC="2")
add_analog("MIN_Temp05","REAL", PV_PREC="2")

add_analog("MAX_Pres01","REAL", PV_PREC="2")
add_analog("MAX_Pres02","REAL", PV_PREC="2")
add_analog("MAX_Pres03","REAL", PV_PREC="2")
add_analog("MAX_Pres04","REAL", PV_PREC="2")
add_analog("MAX_Pres05","REAL", PV_PREC="2")
add_analog("MIN_Pres01","REAL", PV_PREC="2")
add_analog("MIN_Pres02","REAL", PV_PREC="2")
add_analog("MIN_Pres03","REAL", PV_PREC="2")
add_analog("MIN_Pres04","REAL", PV_PREC="2")
add_analog("MIN_Pres05","REAL", PV_PREC="2")

add_analog("MAX_Lvl01","REAL",  PV_PREC="2")
add_analog("MAX_Lvl02","REAL",  PV_PREC="2")
add_analog("MAX_Lvl03","REAL",  PV_PREC="2")
add_analog("MIN_Lvl01","REAL",  PV_PREC="2")
add_analog("MIN_Lvl02","REAL",  PV_PREC="2")
add_analog("MIN_Lvl03","REAL",  PV_PREC="2")

add_analog("MAX_Flow01","REAL", PV_PREC="2")
add_analog("MAX_Flow02","REAL", PV_PREC="2")
add_analog("MAX_Flow03","REAL", PV_PREC="2")
add_analog("MAX_Flow04","REAL", PV_PREC="2")
add_analog("MAX_Flow05","REAL", PV_PREC="2")
add_analog("MIN_Flow01","REAL", PV_PREC="2")
add_analog("MIN_Flow02","REAL", PV_PREC="2")
add_analog("MIN_Flow03","REAL", PV_PREC="2")
add_analog("MIN_Flow04","REAL", PV_PREC="2")
add_analog("MIN_Flow05","REAL", PV_PREC="2")

#Timers and Filters
add_analog("T0_period", "INT")
add_analog("T1_period", "INT")
add_analog("T2_period", "INT")
add_analog("T3_period", "INT")
add_analog("T4_period", "INT")
add_analog("T5_period", "INT")
add_analog("F0_interval", "INT")
add_analog("F0_buffer", "INT")
add_analog("F1_interval", "INT")
add_analog("F1_buffer", "INT")
add_analog("F2_interval", "INT")
add_analog("F2_buffer", "INT")
add_analog("F3_interval", "INT")
add_analog("F3_buffer", "INT")
add_analog("F4_interval", "INT")
add_analog("F4_buffer", "INT")
add_analog("F5_interval", "INT")
add_analog("F5_buffer", "INT")
add_digital("F0_Enable")
add_digital("F1_Enable")
add_digital("F2_Enable")
add_digital("F3_Enable")
add_digital("F4_Enable")
add_digital("F5_Enable")


add_analog("ColdCondLevelMIN",      "REAL",  PV_PREC="2",       PV_DESC="Min Cold Conditioning Level")
add_analog("ColdCondPressureMAX",   "REAL",  PV_PREC="2",       PV_DESC="Max Cold Conditioning Pressure")
add_analog("ColdCondCavTempMAX",    "REAL",  PV_PREC="2",       PV_DESC="Max Cold Conditioning Cav Temp")
add_analog("CondFPCTempMAX",        "REAL",  PV_PREC="2",       PV_DESC="Max Conditioning FPC Temp")
add_analog("CondWtrCTempMAX",       "REAL",  PV_PREC="2",       PV_DESC="Max Conditioning Water Temp")
add_analog("CondWtrCFlowMAX",       "REAL",  PV_PREC="2",       PV_DESC="Max Conditioning Water Flow")
add_analog("CondWtrCFlowMIN",       "REAL",  PV_PREC="2",       PV_DESC="Min Conditioning Water Flow")
add_analog("P_WtrCFS_Ftime",        "REAL",  PV_PREC="2",       PV_DESC="Water FS filter time")

#Water flush
add_analog("Cond_WF_MIN_Temp",      "REAL",  PV_PREC="2",       PV_DESC="Min Temp Water to Flush",  PV_EGU="K")
add_time("Cond_WF_BlowTime",                                    PV_DESC="Time to wait until Water Flush",  PV_EGU="ms")
add_time("Cond_WF_WaitTime",                                    PV_DESC="Time to wait until Water Flush",  PV_EGU="ms")

#Interlock Parameters
add_analog("NightLevel_INTLK",      "REAL",  PV_PREC="2",       PV_DESC="Interlock for Night Mode")
add_analog("NightPressure_INTLK",   "REAL",  PV_PREC="2",       PV_DESC="Interlock for Night Mode")
add_analog("MotorTemp_INTLK",       "REAL",  PV_PREC="2",       PV_DESC="Interlock for Motor OK")
add_analog("EHCavTemp_INTLK",       "REAL",  PV_PREC="2",       PV_DESC="Interlock for Cav Heaters")
add_analog("EHCavTemp_RST",         "REAL",  PV_PREC="2",       PV_DESC="Interlock Reset for Cav Heaters")
add_analog("EHFPC1Temp_INTLK",      "REAL",  PV_PREC="2",       PV_DESC="Interlock for FPC Heaters 1")
add_analog("EHFPC1Temp_RST",        "REAL",  PV_PREC="2",       PV_DESC="Interlock Reset for FPC Heaters 1")
add_analog("EHFPC2Temp_INTLK",      "REAL",  PV_PREC="2",       PV_DESC="Interlock for FPC Heaters 2")
add_analog("EHFPC2Temp_RST",        "REAL",  PV_PREC="2",       PV_DESC="Interlock Reset for FPC Heaters 2")
add_analog("CV31311Pres_INTLK",     "REAL",  PV_PREC="2",       PV_DESC="Interlock for TICP Valve")
add_analog("CV31311Pres_RST",       "REAL",  PV_PREC="2",       PV_DESC="Interlock Reset for TICP Valve")

#Spare
add_digital("paramDI_001")
add_digital("paramDI_002")
add_digital("paramDI_003")
add_digital("paramDI_004")
add_digital("paramDI_005")
add_digital("paramDI_006")
add_digital("paramDI_007")
add_digital("paramDI_008")
add_digital("paramDI_009")
add_digital("paramDI_010")

add_analog("paramAI_001","REAL", PV_PREC="2")
add_analog("paramAI_002","REAL", PV_PREC="2")
add_analog("paramAI_003","REAL", PV_PREC="2")
add_analog("paramAI_004","REAL", PV_PREC="2")
add_analog("paramAI_005","REAL", PV_PREC="2")
add_analog("paramAI_006","REAL", PV_PREC="2")
add_analog("paramAI_007","REAL", PV_PREC="2")
add_analog("paramAI_008","REAL", PV_PREC="2")
add_analog("paramAI_009","REAL", PV_PREC="2")
add_analog("paramAI_010","REAL", PV_PREC="2")
add_analog("MeasureSelector","INT")

############################
#  MESSAGE BLOCK
############################

add_string("MESSAGE_01", 39)
add_string("MESSAGE_02", 39)
add_string("MESSAGE_03", 39)
add_string("MESSAGE_04", 39)
add_string("MESSAGE_05", 39)

#Others
add_string("message",    39,     PV_NAME="message",   	 PV_DESC="Text message")
